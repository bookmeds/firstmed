import React,{Component}from "react"
import { View,StatusBar,Image,AsyncStorage ,Text,ScrollView,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import images from './../BaseClass/Images'
import SplashScreen from 'react-native-splash-screen'
import { CommonActions } from '@react-navigation/native';

import { SafeAreaView } from 'react-native-safe-area-context';

const { width,height } = Dimensions.get('window');

import Db from "./../DB/Realm";

class Intro extends Component
{

    static navigationOptions = 
    {
        header: null,
  
    };
    
    constructor(props)
    {
        super(props);
        this.state={
            dataArray : [
                {
                title:"Order your OTC and Prescribed medicines",
                subtitle:"Order OTC medicines and prescribed drugs, find substitutes, check the usage and side effects of medicines before taking them.",
                image:images.intro1
                },
                {
                title:"Store your health records digitally",
                subtitle:"Take a snap of the diagnostic reports, prescriptions and lab reports and save in the app for hassle-free carrying of hard copies.",
                image:images.intro2
                },
                {
                title:"Chat with our pharmacists",
                subtitle:"chat with our pharmacist team and get your queries resolved.",
                image:images.intro3

                }
            ],
            currentIndex:0
        }
    }

    componentDidMount() {
        

// let dogs = Db.objects('Person'); // retrieves all Dogs from the Realm

// console.log(dogs);
//   Db.write(() => {
//   Db.delete(dogs);

//   })



//         Db.write(() => {
//   // optional properties can be set to null or undefined at creation
//   let charlie = Db.create('Person', {
//     realName: 'Charlie',
//     displayName: null, // could also be omitted entirely
//     birthday: new Date(1995, 11, 25),
//   });

//   // optional properties can be set to `null`, `undefined`,
//   // or to a new non-null value
//   charlie.birthday = undefined;
//   charlie.displayName = 'Charles';

//   // Setting a non-optional property to null will throw `TypeError`
//   // charlie.realName = null;
// });

let userData = Db.objects('User'); 
if(userData.length>0)
{
     global.userData = userData[0]
global.CustomerId = userData[0].CustomerId


     this.props.navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'Home',
                        },
                      ],
                    })
                  );
}
else
{
    SplashScreen.hide();
}






        // AsyncStorage.getItem('CustomerId', (error, result) => {
        //     if(result != null)
        //     {
        //         global.CustomerId = result
        //         // SplashScreen.hide();

        //         this.props.navigation.dispatch(
        //             CommonActions.reset({
        //               index: 0,
        //               routes: [
        //                 {
        //                   name: 'Home',
        //                 },
        //               ],
        //             })
        //           );
        //     }
        //     else
        //     {

        //         SplashScreen.hide();

        //     }
                        
        // })

    }
    
    
    render()
    {
        return(
            <SafeAreaView style = {styles.container}>
                  <StatusBar
                    translucent
                    barStyle="light-content"
                    //  backgroundColor="rgba(0, 0, 0, 0.251)"
                    backgroundColor={Constant.headerColor()}
                 />

                <View style ={styles.pageNoStyle}>
                {this.rendernumbers()}
                </View>
                {this.renderScreens()}
                


                <TouchableOpacity style = {styles.startView}
                onPress={()=>{

                   
                    // AsyncStorage.setItem('mobileNumber', JSON.stringify({name:'uday',id:'141'}));

                    // AsyncStorage.getItem('mobileNumber', (error, result) => {
                    //     alert(JSON.stringify(result))
                    // })
                    
                    this.props.navigation.navigate('Login')
                }}
                >
                       <View style = {styles.startInside}>
                   <Text style = {[styles.startBtn,BaseStyle.mediumFont]}>Get Started</Text>
                   </View>
                </TouchableOpacity>



            </SafeAreaView>
        )
    }

    rendernumbers()
    {
          const {dataArray,currentIndex} = this.state

          return(

                <ScrollView horizontal={true}>
               {
                  dataArray.map((item, index) => (
                  
                   <View key={index} style = {styles.pageStyle}>
                    <Text style = {[styles.pageCount,BaseStyle.mediumFont,{color:index == currentIndex ? Constant.appColorAlpha() : 'gray'}]}>{index+1}</Text>
                    {index == currentIndex ?
                    <Text style = {styles.dotStyle}></Text> : null}
                    </View> 
                  ))
               }
            </ScrollView>
)
    }

    _onMomentumScrollEnd = ({ nativeEvent }: any) => {
   const position = nativeEvent.contentOffset; 
   const index = Math.round(nativeEvent.contentOffset.x / width);

    this.setState({currentIndex:index})
 };


    renderScreens()
    {
        const {dataArray} = this.state

          return(

                <ScrollView horizontal={true}
                snapToInterval={width}
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                decelerationRate={50}
                snapToAlignment={"center"}
                 onScroll={this._onMomentumScrollEnd}
               >
               {
                  dataArray.map((item, index) => (
                   <View key={index} style = {styles.scrrenStyle}>
                    <Text key={index+'text'} style ={[styles.title,BaseStyle.boldFont]}>{item.title}</Text>
                    <Text key={index+'text1'} style ={[styles.subtitle,BaseStyle.regularFont]}>{item.subtitle}</Text>
                    <View style = {{flex:1,paddingBottom:80,alignItems:'center',justifyContent:'center'}}>
                    <Image resizeMode={'contain'} style = {{width:width-60,height:'85%' }} source={item.image}/>
                    </View>

                    </View> 
                  ))
               }
            </ScrollView>
)
        
}
}


const styles = StyleSheet.create({
 container:{
     flex: 1,
     backgroundColor:'#fff'
 },
 scrrenStyle:{
     marginTop:10,
    width:width,
    height:'100%',
 },
 pageNoStyle:
 {
     width:'100%',
     height:50,
     marginTop:15,
     alignItems:'center',
     justifyContent: 'center',
 },
 pageStyle:{
     width:60,
     height:'100%',
     alignItems:'center',
     justifyContent: 'center',
 },
 dotStyle:{
     marginTop:5,
     height:6,
     width:6,
     backgroundColor:Constant.appColorAlpha(),
     borderRadius:3
 },
 title:{
     marginLeft:20,marginRight:20,lineHeight:30,
     fontSize:22,
     color:Constant.appColorAlpha()
 },
 subtitle:{
     marginTop:15, marginLeft:15,marginRight:15,lineHeight:20,
      color:Constant.appColorAlpha()

 },
 pageCount:{
     fontSize:16
 },
 startView:{
    position: 'absolute',
    bottom:25,left:15,right:15,
    borderRadius : 25,
    height:50,
 },
 startInside:{
    width:'100%',
    height:'100%',
    borderRadius:25,
    backgroundColor:Constant.appColorAlpha(),
    alignItems:'center',justifyContent:'center'
 },
 startBtn:
 {
     color:'#fff',
     fontSize:16

 }

 
});

export default Intro