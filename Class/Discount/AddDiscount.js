import React,{Component}from "react"
import { View,TextInput,ImageBackground,AppState,Text,ScrollView,Image,FlatList,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import images from "./../BaseClass/Images";
import { SafeAreaView } from "react-native-safe-area-context";
import  SwipeListView  from './../ExtraClass/SwipeList/SwipeListView';
import SplashScreen from 'react-native-splash-screen'
import Db from "./../DB/Realm";
const { width,height } = Dimensions.get('window');
import moment from 'moment';
import ImageLoad from './../BaseClass/ImageLoader'
import urls from "./../BaseClass/ServiceUrls";
import Toast from 'react-native-simple-toast';

class AddDiscount extends Component
{


    constructor(props)
    {
        super(props);
        this.state={
          isSpeaking:false,
          discountArray:[],
          code:'',
          isLoading:false
    }
}

    componentDidMount()
    {
      SplashScreen.hide();
    let DiscountData = Db.objects('BestDiscount').filtered('CouponCode != null AND EndDateUtc > $0',new Date())
    this.setState({discountArray:DiscountData})


    }


   
   render()
    {
       const {article,isSpeaking} = this.state
       
    
        const {discountArray,isLoading} = this.state
      return(
          <SafeAreaView style = {{flex:1,backgroundColor:'#fff'}}>

                <View style = {{backgroundColor:Constant.headerColor(),elevation: 4 ,borderBottomLeftRadius:30,borderBottomRightRadius:30}}>

                <View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',borderBottomLeftRadius:30,borderBottomRightRadius:30}}>
                <TouchableOpacity
                onPress = {()=>{
                    this.props.navigation.pop(1)
                }}
                style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
                <Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
                </TouchableOpacity>
                <Text style = {[{flex:1},BaseStyle.headerFont]}>OFFERS FOR YOU</Text>

                </View>

                </View>

                {isLoading ? Constant.showLoader() : null}

            <View style = {{width:'100%',height:130,elevation:4,backgroundColor:'#fff',padding:10}}>
             <Text style = {[{fontSize:15,},BaseStyle.regularFont]}>REDEEM HERE</Text>
      <TextInput
        style={{height: 40,color:Constant.appColorAlpha()}}
        placeholder="Enter Code"
        onChangeText={(text) => {this.setState({code:text})}}
        defaultValue={''}
        autoFocus
      />
      <View style = {{width:'100%',height:1,backgroundColor:Constant.appColorAlpha()}}/>

     <Text onPress={()=>{
         this.applyCoupon(code)
     }} style = {[{fontSize:16,textAlign:'right',marginTop:18,color:Constant.appColorAlpha()},BaseStyle.boldFont]}>Apply</Text>


     </View>


            {discountArray.length > 0 ? <Text style = {[{fontSize:15,marginLeft:12,marginTop:10},BaseStyle.regularFont]}>CHOOSE FROM THE OFFERS BELLOW</Text>
            : null }
            <FlatList
            data={discountArray}
            keyExtractor={item => item.name}
            renderItem={this.renderItem}
           
        />

                </SafeAreaView>
      )
    }

    renderItem = ({item,index}) =>{
            
            const {discountArray} = this.state
            return (
                <View key={index} style = {{width:'100%', padding:10,marginBottom: index == discountArray.length-1 ? 50 : 0 }}>
                <View style  ={{elevation:4,borderRadius:8,backgroundColor:'#fff'}}>
                <View style = {{flexDirection:'row'}}>
                  <ImageLoad
                    placeholderStyle={{width:100,height:100}}
                    style ={{width:100,height:100}}
                    loadingStyle={{ size: 'large', color: Constant.appColorAlpha() }}
                    source={{ uri:item.PictureUrl}}
                    placeholderSource={images.placeholder}
                />
                 <View style = {{flex:1,marginTop:15,justifyContent:'space-between',marginBottom:15}}>
                     <View>
                     <Text style  = {[ {fontSize:18,color:Constant.appColorAlpha()},BaseStyle.boldFont]}>{item.Name}</Text>
                     <View style = {{flexDirection:'row',alignItems:'center'}}>
                     <Image resizeMode={'center'} style = {{width:30,height:30}} source = {images.stopwatch}/>
                     <View style = {{marginLeft:6}}>
                     <Text style  = {[ {fontSize:13,marginTop:2,color:Constant.textColor()},BaseStyle.regularFont]}>Promo Period</Text>
                     <Text style  = {[ {fontSize:13,color:'#000'},BaseStyle.boldFont]}>{moment(item.StartDateUtc).format('DD MMM')} - {moment(item.EndDateUtc).format('DD MMM YYYY') }</Text>
                     </View>
                     </View>

                     <View style={{backgroundColor:'#fff',borderColor:'black',marginTop:6, width:'70%',borderWidth:1, borderRadius:6,borderStyle: "dashed",alignItems:'center',justifyContent:'center',paddingTop:10,paddingBottom:10}}>
                           <Text style  = {[ {fontSize:13,color:Constant.appColorAlpha()},BaseStyle.boldFont]}>{item.CouponCode}</Text>
                     </View>
                     </View>
                     
                 </View>
                 </View>
                 

                 
                       <View style = {{width:'100%',paddingTop:5,marginBottom:10 ,justifyContent:'center',alignItems:'center' }}>
                        <TouchableOpacity 
                        onPress={()=>{
                            this.applyCoupon(item.CouponCode)
                        }}
                        style = {{height:34,width:120,justifyContent:'center',alignItems:'center' ,backgroundColor:Constant.appColorAlpha(),borderRadius:17,elevation:4}}>
                        <Text style  = {[ {fontSize:13,color:'#fff'},BaseStyle.boldFont]}>APPLY COUPON</Text>
                        </TouchableOpacity>
    
                     </View>
                
    
                 </View>

                 
                </View>
            )
    }

   
   applyCoupon(code)
  {
      this.setState({isLoading:true})
  let params = {
        "CustomerId":global.CustomerId,
        "CouponCode":code}
    Constant.postMethod(urls.applyDiscountCoupon,params, (result) => {
      
      this.setState({isLoading:false})
      if(result.success)
      {
        if(result.result.Response != null)
        {
// this.props.navigation.pop(1)
            let couponData = result.result.Response
            // alert(JSON.stringify(couponData.OrderTotalsModel != null ? true : false))
            if(couponData.OrderTotalsModel != null)
            {   
            
                if(this.props.route.params != null)
                {
                this.props.route.params.selectedDiscount(couponData.OrderTotalsModel,code)
                }
                this.props.navigation.goBack(null)
                
            }

             Toast.show(couponData.Message != null ? couponData.Message : '')


        }
      }


    
      

    })
  }


}
export default AddDiscount
