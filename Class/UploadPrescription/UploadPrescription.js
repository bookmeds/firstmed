import React,{Component}from "react"
import { View,Text, Alert,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import SplashScreen from 'react-native-splash-screen'
import { FlatList, TextInput } from "react-native-gesture-handler";
import images from "./../BaseClass/Images";
import { SafeAreaView } from "react-native-safe-area-context";
const { width,height } = Dimensions.get('window');
import language from './../BaseClass/language'

class UploadPrescription extends Component
{

    
    constructor(props)
    {
        super(props);
        this.state={
            isLoading:false,
            searchValue:'',
            showWhy:true,
            showValid:true
}}


 componentDidMount() {
        SplashScreen.hide();
        // this._unsubscribe = this.props.navigation.addListener('focus', () => {
        //     this.getAddress()
        //   })


    }
  render()
  {
        const {isLoading , searchValue,showWhy,showValid } = this.state

      return(
          <SafeAreaView  style = {{flex:1,backgroundColor:'#fff'}}>
              {isLoading ? Constant.showLoader() : null}

            
              <View style = {{backgroundColor:Constant.headerColor() ,borderBottomLeftRadius:30,borderBottomRightRadius:30}}>

            <View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',borderBottomLeftRadius:30,borderBottomRightRadius:30}}>
           <TouchableOpacity
              onPress = {()=>{
                  this.props.navigation.pop(1)
              }}
               style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
              <Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
              </TouchableOpacity>
              <Text style = {[{flex:1},BaseStyle.headerFont]}>{language.uploadPres.toUpperCase()}</Text>

              
                </View>
              <View style = {{height:40,width:'100%'}}/>

            

              </View>
            <ScrollView style = {{marginTop:-30}}>
              <View style = {styles.mainView}>
                  <View style = {{flexDirection:'row',height:90,width:'100%',justifyContent:'space-between',paddingLeft:8,paddingRight:8}}>
                      <View style = {{flex:0.22,alignItems:'center',justifyContent:'center',elevation:4,backgroundColor:'#fff' ,borderRadius:15}}>
                      <View style = {styles.itemImage}>
                        <Image resizeMode={'contain'} style = {{width:24,height:24}} source = {images.uCam}/>
                        </View>
                        <Text style = {[{color:Constant.appColorAlpha(),marginTop:5},BaseStyle.regularFont]}>{language.Camera}</Text>
                      </View>

                      <View style = {{flex:0.22,alignItems:'center',justifyContent:'center',elevation:4,backgroundColor:'#fff' ,borderRadius:15}}>

                      <View style = {styles.itemImage}>
                        <Image resizeMode={'contain'} style = {{width:24,height:24}} source = {images.uGallery}/>
                        </View>

                          <Text style = {[{color:Constant.appColorAlpha(),marginTop:5},BaseStyle.regularFont]}>{language.Gallery}</Text>
                      </View>

                      <View style = {{flex:0.22,alignItems:'center',justifyContent:'center',elevation:4,backgroundColor:'#fff' ,borderRadius:15}}>

                      <View style = {styles.itemImage}>
                        <Image resizeMode={'contain'} style = {{width:24,height:24}} source = {images.uReport}/>
                        </View>

                          <Text style = {[{color:Constant.appColorAlpha(),marginTop:5},BaseStyle.regularFont]}>{language.myrecords}</Text>
                      </View>

                       <View style = {{flex:0.22,alignItems:'center',justifyContent:'center',elevation:4,backgroundColor:'#fff' ,borderRadius:15}}>

                      <View style = {styles.itemImage}>
                        <Image resizeMode={'contain'} style = {{width:24,height:24}} source = {images.uReport}/>
                        </View>

                          <Text style = {[{color:Constant.appColorAlpha(),marginTop:8,textAlign:'center',lineHeight:15},BaseStyle.regularFont]}>{language.talkToDoctor}</Text>
                      </View>



                  </View>
                 
                  <View style ={{alignItems:'center',justifyContent:'center',marginTop:20,backgroundColor:'#fff'}}>
                  <Image resizeMode={'contain'} style = {{height:100,width:100}} source = {images.shild}/>
                  <Text style = {[{color:Constant.appColorAlpha(),textAlign:'center',marginTop:5},BaseStyle.regularFont]}>{language.secure}</Text>


                  </View>

                  <View style = {{backgroundColor:'#fff'}}>
                      <TouchableOpacity 
                      activeOpacity={1}
                      onPress={()=>{
                          this.setState({showWhy:!showWhy})
                      }}
                      style = {{width:'100%',marginTop:15}}>
                        <View style = {{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                      <Text style = {[{fontSize:16,flex:1},BaseStyle.boldFont]}>{language.whyPres}</Text>
                      <Image style = {{height:15,width:15,right:10}} source = {showWhy ? images.upArrow : images.downArrow}/>
                      </View>
                      {showWhy ?  <Image resizeMode={'contain'} style = {{height:200,width:'100%'}} source={images.whyP}/> : null}
                      </TouchableOpacity>
                 
                      <TouchableOpacity 
                      activeOpacity={1}
                      onPress={()=>{
                          this.setState({showValid:!showValid})
                      }}
                      style = {{width:'100%',marginTop:15}}>
                                                <View style = {{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                      <Text style = {[{fontSize:16,flex:1},BaseStyle.boldFont]}>{language.validPres}</Text>
                      <Image style = {{height:15,width:15,right:10}} source = {showValid ? images.upArrow : images.downArrow}/>

                      </View>
                      {showValid ?  <Image resizeMode={'contain'} style = {{height:500,width:'100%'}} source={images.validP}/> : null}
                      </TouchableOpacity>
                  </View>


             </View>
                      </ScrollView>




          

         </SafeAreaView>
      )
  }

  
 
}

const styles = StyleSheet.create({

    mainView:{
        marginTop:0,
      
        flex:1,
        paddingLeft:15,paddingRight:15,
    },
    itemImage:{alignItems:'center',justifyContent:'center', height:40,elevation:4 ,width:40,backgroundColor:Constant.appLightColor(),borderRadius:6 },

})

export default UploadPrescription