import React from 'react';
import LocalizedStrings from 'react-native-localization';
const strings = new LocalizedStrings({
  en: {

    //   login
      signin : "Sign in",
      emailpassword : "Email/Phone",
      password : "Password",
      forgotpassword : "Forgot Password",
      dontHaveAccount : "Don't have an account?",
      signup: " SIGN UP ",

      //forgot passoword
      send : "Send",

    //   create pin

    createpin : "Create Pin",
    enterpin : "Enter pin",
    confirmPin : "Confirm pin",
    continue : "Continue",


    // register
    name : 'Name',
    email : 'Email',
    phonenumber : 'Phone number',

    allReadyHaveAccount : 'Allready have an account?',


    // sidemenu
    home:'Home',
    healthrecords:'Health Records',
    addresss:'My Address',
    healthArticles:'Health Articles',
    medicationReminder:'Medication Reminder',
    orders:'Orders',
    version:'Version',

    // home
    searchLocation:'Search Location',
    uploadPres : 'upload prescription',
    chatwith :'Chat with pharmacist',
    categories:'Categories',
    healthissues:'Health Issues',
    viewall:'View All',
    bestoffers:'Best Offers',
    articlesforyou:'Articles for you',
    bestselling:'Best Selling',
    newarrivals:'New Arrivals',


    healthcarerecords:'healthcare records',
    uploadreport:'Upload Report',
    perscription:'Prescriptions',
    diagnosticReport:'Diagnostic report',
    medicalClaims:'Medical claims',
    patientName:'Patient Name',
    doctorname:'Doctor Name',
    reportname:'Report Name',
    reportsummary:'Report Summary',

    save:'SAVE',
    update:'UPDATE',

    patient:'Patient',
    doctor:'Doctor',
    summary:'Summary',
    addNewAddress:'ADD NEW ADDRESS',
   
    type:'Type',
    houseFlat :'House/Flat No',
    landmark :'Landmark',
    city:'City',
    country:'Country',
    zipcode:'ZipCode',
    category:'Category',

    today:'Today',
    week:'Week',
    all:'All',
    bookmark:'Bookmarks',


    // upload prescripción
    Camera:'Camera',
    Gallery:'Gallery',
    myrecords:'My Records',
 secure:'Your attached prescription will be secure and private.Only our pharmacist will review it.',
 whyPres:'Why upload a prescription?',
 validPres:'Valid prescription guide',

 //pill reminders

 pillreminder:'Pill Reminder',
 reminder:'Reminder',
 summary:'Summary',
 morning:'Morning',
 afternoon:'Afternoon',
 evening:'Evening',
 addmedicine:'Add medicine',
 medicineform:'Medicine form',
 shape:'shape',
 color:'color',
 dose:'dose',
 times:'times',
 image:'image',
 timeslot:'select time slots',
 switchView:'Switch View',
 recurring:'recurring',
 selectall:'select all',
 chat:'chat',
 orderMedicine:'Order Medicine',
 setreminder:'Set Reminder',
 searchmedicine:'Search Medicine',
 selectdays:'Select days',

 profile:'Profile',
 seniorDocument:'Senior Citizen Document',
 handicappedDocument:'Handicapped Document',
 bodymass:'Body Mass Index',
 overweight:'Over weight',

 height:'Height',
 weight:'Weight',
 buynow:'Buy now',


 //orders
 orders:'Orders',
 prescription:'Prescription',
 non_pres:'Non-prescription',
 order_no:'Order No',
 status:'Status',
 purchasedate:'Purchase date',
 review:'Your order is under review',
 confirmOrder:'Confirm order',
 medicinesfor:'Medicines for',
 self:'Self',
 Others:'Others',
 instructions:'Instructions',
 discountCode:'Apply Discount \n Coupon',
 change:'Change',
 billDetails:'Bill Details',
 prescriptionMedicines:'Prescription Medicines',
nonPrescriptionMedicines:'Non Prescription Medicines',
subtotal:'Sub Total',
combined:'Combined',
servicefee:'Service Fee',
total:'Total',
ViewPrescription:'View Prescription',
confirmOrder:'Confirm Order',
discount:'Discount',

gender:'Gender',
birthdate:'Birthdate',
bloodGroup:'Blood Group',
browseDocument:'Browse Document',
Add:'Add',



//cart

cart:'My cart',
totalAmount : 'Total Amount',
checkOut : 'Check out',
continueShopping : 'Continue shopping',
next:'Next',
skip:'Skip',
addAddress : 'Add Address',
takePrescription:'Take prescription',
talkToDoctor:'Talk to a doctor'



















 







  },
  sp : {
      //   login
      signin : 'Registrarse',
      emailpassword : "Email / Teléfono",
      password : "Contraseña",
      forgotpassword : "Se te olvidó tu contraseña",
      dontHaveAccount : "No tengo una cuenta?",
      signup: " REGÍSTRATE ",

      //forgot passoword
      send:"enviar",

       //   create pin

    createpin : "Crear pin",
    enterpin : "Ingrese su PIN",
    confirmPin : "Confirmar PIN",
    continue : "Seguir",

     // register
     name : 'Nombre',
     email : 'Correo electrónico',
     phonenumber : 'Número de teléfono',
 
     allReadyHaveAccount : 'Ya tienes una cuenta?',


     // sidemenu
    home:'casa',
    healthrecords:'Registros de salud',
    addresss:'Mi dirección',
    healthArticles:'Artículos de salud',
    medicationReminder:'Recordatorio de medicación',
    orders:'Pedidos',
    version:'versión',
discount:'Descuento',

    searchLocation:'Buscar ubicación',
    uploadPres : 'subir prescripción',
    chatwith :'Chatear con la farmacéutica',
    categories:'Categorias',
    healthissues:'Problemas de salud',
    viewall:'Ver todo',
    bestoffers:'Mejores ofertas',
    articlesforyou:'Artículos para ti',
    bestselling:'Más vendido',
    newarrivals:'Los recién llegados',


    healthcarerecords:'registros de salud',
    uploadreport:'Subir informe',
    perscription:'Prescripción',
    diagnosticReport:'Informe de diagnóstico',
    medicalClaims:'Reclamos médicos',
    patientName:'nombre del paciente',
    doctorname:'nombre del doctor',
    reportname:'Reportar nombre',
    reportsummary:'Resumen del informe',
    save:'salvar',
    update:'ACTUALIZAR',

    patient:'Paciente',
    doctor:'Médica',
    summary:'Resumen',
    addNewAddress:'AGREGAR NUEVA DIRECCIÓN',



    type:'Tipo',
    houseFlat :'Casa / Piso No',
    landmark :'Punto de referencia',
    city:'Ciudad',
    country:'País',
    zipcode:'Código postal',
    category:'Categoría',


    today:'Hoy',
    week:'Semana',
    all:'Todas',
    bookmark:'Marcadores',


      Camera:'Cámara',
    Gallery:'Galería',
    myrecords:'Mis registros',
    secure:'Su receta adjunta será segura y privada.Solo nuestro farmacéutico lo revisará.',
    whyPres:'por qué subir una receta?',
    validPres:'Guía de prescripción válida',



 pillreminder:'Recordatorio de pastillas',
 reminder:'Recordatorio',
 summary:'Resumen',
 morning:'Mañana',
 afternoon:'Tarde',
 evening:'Noche',
 addmedicine:'Añadir medicina',
 medicineform:'Formulario de medicina',
 shape:'forma',
 color:'color',
 dose:'dosis',
 times:'veces',
 image:'imagen',
 timeslot:'seleccionar franjas horarias',
 switchView:'Cambiar vista',
 recurring:'periódica',
 selectall:'seleccionar todo',
 chat:'charla',
orderMedicine:'Medicina de la orden',
setreminder:'Establecer recordatorio',
searchmedicine:'Buscar medicina',
selectdays:'Seleccione dias',


 bodymass:'Índice de masa corporal',
 handicappedDocument:'Documento para discapacitados',
 seniorDocument:'Documento de la tercera edad',
 profile:'perfil',
 overweight:'Exceso de peso',

height:'Altura',
weight:'Peso',
buynow:'Compra ahora',


//orders
 orders:'Pedidos',
 prescription:'Prescripción',
 non_pres:'No prescripción',
 order_no:'N º de pedido',
 status:'Estado',
 purchasedate:'Fecha de compra',
 review:'Su pedido está bajo revisión',
 confirmOrder:'Confirmar pedido',
 medicinesfor:'Medicamentos para',
 self:'Yo',
 Others:'Otras',
 instructions:'Instrucciones',
 discountCode:'Aplicar cupón de \n descuento',
 change:'Cambio',
 billDetails:'Detalles de factura',
 prescriptionMedicines:'Medicamentos recetados',
nonPrescriptionMedicines:'Medicamentos sin receta',
subtotal:'Subtotal',
combined:'Conjunta',
servicefee:'Tarifa de servicio',
total:'Total',
 prescriptionMedicines:'Medicamentos recetados',
ViewPrescription:'Ver receta',
confirmOrder:'Confirmar pedido',
gender:'Género',
birthdate:'Fecha de nacimiento',
bloodGroup:'Grupo sanguíneo',
browseDocument:'Examinar documento',
Add:'Añadir',
cart:'Mi carrito',
totalAmount : 'Cantidad total',
checkOut : 'Revisa',
continueShopping : 'Seguir comprando',
next:'Próxima',
skip:'Omitir',
addAddress : 'Añadir dirección',
takePrescription:'Tomar prescripción',
talkToDoctor:'He aceptado la oferta'




  }
})
export default strings;