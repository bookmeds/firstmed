
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, PanResponder, Animated, ScrollView } from 'react-native';
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import Constant from './../BaseClass/Constant'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


type Props = {
    /**
   
     * Method for rendering non-scrollable header of bottom sheet.
     */
    renderHeader?: () => React.ReactNode
}
export default class App extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        stopScroll:false,
        isTop:false,
        offset: {}

    }
    const {height, width} = Dimensions.get('window');

    const initialPosition = {x: 0, y: height - 110}
    const position = new Animated.ValueXY(initialPosition);

    const parentResponder = PanResponder.create({
      onMoveShouldSetPanResponderCapture: (e, gestureState) => {
        return true
      },
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: (e, gestureState) =>  {
        if (this.state.toTop) {
          return false
        } else {
          return gestureState.dy < -6
        }
      },
      onPanResponderTerminationRequest: () => false,
      onPanResponderMove: (evt, gestureState) => {
        let newy = gestureState.dy
        if (this.state.toTop && newy < 0 ) return
        if (this.state.toTop) {
          position.setValue({x: 0, y: newy});
        } else {
          position.setValue({x: 0, y: initialPosition.y + newy});
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (this.state.toTop) {
          if (gestureState.dy > 50) {
            this.snapToBottom(initialPosition)
          } else {
            this.snapToTop()
          }
        } else {
          if (gestureState.dy < -90) {
            this.snapToTop()
          } else {
            this.snapToBottom(initialPosition)
          }
        }
      },
    });

    this.offset = 0;
    this.parentResponder = parentResponder;
    this.state = { position, toTop: false };
  }

  snapToTop = () => {
    Animated.timing(this.state.position, {
      toValue: {x: 0, y: 0},
      duration: 300,
    }).start(() => {

this.props.open(true)  
  });
    this.setState({ toTop: true })
  }

  snapToBottom = (initialPosition) => {
    Animated.timing(this.state.position, {
      toValue: initialPosition,
      duration: 150,
    }).start(() => {
        this.props.close(true)  
    });
    this.setState({ toTop: false})
  }

  hasReachedTop({layoutMeasurement, contentOffset, contentSize}){
    return contentOffset.y == 0;
  }


  render() {
    const {height} = Dimensions.get('window');

    const {
        renderHeader,
         children,
       } = this.props;

    return (

        
      <View style={styles.container}>
        <Animated.View style={[styles.draggable, {backgroundColor:'#fff', borderRadius:10,elevation:10,  height:height }, this.state.position.getLayout()]} {...this.parentResponder.panHandlers}>
            <View style = {{height:30,width:'100%',alignItems:'center',justifyContent:'center'}}>
          <Text style={styles.dragHandle}></Text>
          </View>
          <Animated.View style = {{zIndex: 101}} {...this.parentResponder.panHandlers}>
          {this.props.renderHeader && this.props.renderHeader()}
          </Animated.View>

          <KeyboardAwareScrollView extraHeight={90} extraScrollHeight={50}>
            <View>
                     {children}
                     </View>
</KeyboardAwareScrollView>

        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    justifyContent: 'center',
    flexDirection:'column-reverse',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
  },
  
  draggable: {
      position: 'absolute',
      right: 0,
      left:0,
      backgroundColor: '#fff',
      alignItems: 'center'
  },
  dragHandle: {
   width:40,
   height:8,
   backgroundColor:Constant.appColorAlpha(),
   borderRadius:4
  },
  scroll: {
      backgroundColor:'#fff'
  }
});