import React,{Component}from "react"
import { View,ImageBackground,AppState,Text,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from './../BaseClass/BaseStyles'
import  Constant from './../BaseClass/Constant'
import images from "./../BaseClass/Images";
import { SafeAreaView } from "react-native-safe-area-context";
import  SwipeListView  from './../ExtraClass/SwipeList/SwipeListView';
import { CommonActions } from '@react-navigation/native';

const { width,height } = Dimensions.get('window');


class SuccessScreen extends Component
{


    constructor(props)
    {
        super(props);
        this.state={
          isSpeaking:false,
        
    }
}

    componentDidMount()
    {
        this.interval = setInterval(
            () => 
            {
              
                clearInterval(this.interval);


                this.props.navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'Home',
                        },
                      ],
                    })
                  );

               
            },
            1500
          );

    }


   
   render()
    {
       const {article,isSpeaking} = this.state
       
    
        const {wishlistArray} = this.state
      return(
          <SafeAreaView style = {{flex:1,backgroundColor:'#fff'}}>

                <View style = {{backgroundColor:Constant.headerColor(),elevation: 4 ,borderBottomLeftRadius:30,borderBottomRightRadius:30}}>

                <View style = {{paddingTop:10,paddingBottom:15, backgroundColor:Constant.headerColor() , flexDirection:'row',alignItems:'center',justifyContent:'center',borderBottomLeftRadius:30,borderBottomRightRadius:30}}>
                <TouchableOpacity
                onPress = {()=>{
                    this.props.navigation.pop(1)
                }}
                style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
                <Image tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.back}/>
                </TouchableOpacity>
                <Text style = {[{flex:1},BaseStyle.headerFont]}>ORDER PLACED</Text>

                </View>

                </View>


                <View style ={{alignItems:'center',justifyContent:'center',flex:1}}>
                <Image resizeMode={'contain'} style = {{height:width/3,width:width/3}} source = {images.success}/>
                <Text style = {[{fontSize:16,marginTop:10},BaseStyle.regularFont]}>Order placed successfully</Text>

                </View>
                </SafeAreaView>



      )
    }


}
export default SuccessScreen
